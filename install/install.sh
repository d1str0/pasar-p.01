#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
        echo 'Este script se debe ejecutar con sudo o como root' >&2
        exit 1
fi

echo -e "Voy a configurar el sistema con parametros de pasarConf.json e instalar dependencias\n"
while true
do
	read -p "Seguro que pasarConf.json contine los parámetros correctos? (y/n)" yn
	case $yn in
		[Yy]* ) break;;
		[Nn]* ) echo -e "\n No he hecho cambios"; exit;;
		* ) echo -e "\nque?";;
	esac
done

echo "Actualizando ..."
sleep 2
apt update

echo "Instalando dependencias ..."
sleep 2
apt -y install ffmpeg curl jq

wifiConfig="/etc/wpa_supplicant/wpa_supplicant.conf"
read dirRaiz ssid wPass< <(echo $(cat pasarConf.json |jq -r '.dirRaiz, .ssid, .wPass'))

echo -e "\nConfigurando servicios:"
echo -e "Led integrado... led.service\n"

sed "s|\(Description=\)|&Controla el led integrado|
s|\(ExecStart=\)|&/bin/bash -c '${dirRaiz}led.sh'|
s|\(WorkingDirectory=\)|&$dirRaiz|
s|\(Restart=\)|&always|
 " template > led.service
chmod +x led.service
cp led.service /etc/systemd/system

echo -e "Apagado del sistema... apagado.service\n"

sed "s|\(Description=\)|&Controla el apagado del sistema|
s|\(ExecStart=\)|&/bin/bash -c '${dirRaiz}apagado.sh'|
s|\(WorkingDirectory=\)|&$dirRaiz|
s|\(Restart=\)|&always|
 " template > apagado.service
chmod +x apagado.service
cp apagado.service /etc/systemd/system

echo -e "Configuracion al bootear... config.service\n"

sed "s|\(Description=\)|&Configura cron de root y wifi|
s|\(ExecStart=\)|&/bin/bash -c '${dirRaiz}config.sh'|
s|\(WorkingDirectory=\)|&$dirRaiz|
s|\(Restart=\)|&no|
 " template > config.service
chmod +x config.service
cp config.service /etc/systemd/system

echo "Copiando config a /boot"
cp pasarConf.json /boot

echo -e"\n Configurando systemd"

systemctl enable led.service apagado.service config.service
echo

echo -e"\nConfigurando WiFi"

if grep -w "$ssid" $wifiConfig 
then
	sed -i $"/$ssid/!b;n;c\ \psk=\"$wPass\"" $wifiConfig
else
	echo -e "\nnetwork={\n ssid=\"$ssid\"\n psk=\"$wPass\"\n}">> $wifiConfig
fi

while true
do
	read -p "\n Configuración terminada. Reiniciar? (y/n)" yn
	case $yn in
		[Yy]* ) echo -e "\nReiniciando... \n"; sleep 2; reboot; exit;;
		[Nn]* ) echo -e "\nReiniciar manualmente"; exit;;
		* ) echo -e "\nque?";;
	esac
done
