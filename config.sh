#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
        echo 'Este script se debe ejecutar con sudo o como root' >&2
        exit 1
fi

wifiConfig="/etc/wpa_supplicant/wpa_supplicant.conf"

read user capHabilitada upHabilitado dirRaiz logHabilitado dirLog ssid wPass< <(echo $(cat /boot/pasarConf.json |jq -r '.user, .capHabilitada, .upHabilitado, .dirRaiz, .logHabilitado, .dirLog, .ssid, .wPass'))

intervaloCap="$(cat /boot/pasarConf.json |jq -r '.intervaloCap')"
intervaloUp="$(cat /boot/pasarConf.json |jq -r '.intervaloUp')"

if grep -w "$ssid" $wifiConfig 
then
	sed -i $"/$ssid/!b;n;c\ \psk=\"$wPass\"" $wifiConfig
else
	echo -e "\nnetwork={\n ssid=\"$ssid\"\n psk=\"$wPass\"\n}">> $wifiConfig
fi

if [[ $capHabilitada == true ]]
then
	capHabilitada=""
else
	capHabilitada="#"
fi

if [[ $upHabilitado == true ]]
then
	upHabilitado=""
else
	upHabilitado="#"
fi

if [[ $logHabilitado == true ]]
then 
	log=">> "$dirRaiz$dirLog"cron.log 2>&1"
else
	log=">/dev/null 2>&1"
fi

# Agrega trabajos a cron. "-e" permite usar "\n" como quiebre de línea
echo -e "$capHabilitada$intervaloCap su $user -c \"$dirRaiz"captura.sh" $log\"\n$upHabilitado$intervaloUp su $user -c \"$dirRaiz"upload.sh" $log"\" | crontab - 

cat $wifiConfig
echo
crontab -l
