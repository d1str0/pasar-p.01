#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
        echo 'Este script se debe ejecutar con sudo o como root' >&2
        exit 1
fi
echo none >/sys/class/leds/led0/trigger

dirRaiz="$(cat /boot/pasarConf.json |jq -r '.dirRaiz')"

while true
do
	read t1 t2 < <(echo $(cat $dirRaiz"led.json" |jq -r '.t1, .t2'))
	sleep $t1
	echo 1 >/sys/class/leds/led0/brightness
	sleep $t2
	echo 0 >/sys/class/leds/led0/brightness
done
