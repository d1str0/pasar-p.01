#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
        echo 'Este script se debe ejecutar con sudo o como root' >&2
        exit 1
fi

read user dirRaiz logHabilitado dirLog dirUpload< <(echo $(cat /boot/pasarConf.json |jq -r '.user, .dirRaiz, .logHabilitado, .dirLog, .dirUpload'))

intervaloUp="$(cat /boot/pasarConf.json |jq -r '.intervaloUp')"

if [[ $logHabilitado == true ]]
then 
	log=">> "$dirRaiz$dirLog"cron.log 2>&1"
else
	log=">/dev/null 2>&1"
fi

pin="3"

#   Exporta el pin al userspace
echo $pin > /sys/class/gpio/export                  

# Setea el pin como input
echo "in" > /sys/class/gpio/gpio$pin/direction

while true
do
	estado=$(cat /sys/class/gpio/gpio$pin/value)

	if [ $estado == 0 ]  
	then

	jq -n --arg t1 ".05" --arg t2 ".1" '{"t1":$t1, "t2":$t2}' > $dirRaiz"led.json"
	chown $user:$user $dirRaiz"led.json"

	wall -n "Esperando a que se cierren todos los procesos"

	echo -e "$intervaloUp su $user -c \"$dirRaiz"upload.sh" $log"\" | crontab - 
	break

	fi
	sleep 2
	
done

while true
do

	if [ ! $(pgrep ffmpeg) ] && [ ! "$(ls $dirRaiz$dirUpload)" ] 
	then
			
		wall -n "Apagando"
		jq -n --arg t1 "1" --arg t2 "1" '{"t1":$t1, "t2":$t2}' > $dirRaiz"led.json"
		chown $user:$user $dirRaiz"led.json"
		shutdown -h now
		break
	else
		if [ -z $avisado ]
		then
			wall -n "Existen procesos activos"
			avisado="true"
		fi

	fi

	sleep 1
done
