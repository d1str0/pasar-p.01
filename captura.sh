#!/bin/bash

if [ $(pgrep ffmpeg) ]
then
	echo "Existe proceso ffmpeg anterior!"
	exit 1;
fi
jq -n --arg t1 ".5" --arg t2 ".25" '{"t1":$t1, "t2":$t2}' > $dirRaiz"led.json"

read nombre duracion resolucion dirCamara dirRaiz dirCaptura dirEncode dirUpload< <(echo $(cat /boot/pasarConf.json |jq -r '.nombre, .duracion, .resolucion, .dirCamara, .dirRaiz, .dirCaptura, .dirEncode, .dirUpload'))

# ts=$(date +$nombre)

# timestamp en unix epoch. Formato hardcodeado
ts=$(($(date +%s%N)/1000000))
nombre=$ts".mp4"

ffmpeg -f video4linux2 -video_size $resolucion -input_format mjpeg -i $dirCamara -an -c copy -t $duracion $dirRaiz$dirCaptura$nombre

ffmpeg -i $dirRaiz$dirCaptura$nombre -vcodec libx264 -crf 23 -preset ultrafast $dirRaiz$dirEncode$nombre
ffmpeg -ss 00:00:01 -i $dirRaiz$dirCaptura$nombre -frames:v 1 $dirRaiz$dirEncode$ts.jpg 

mv $dirRaiz$dirEncode$nombre $dirRaiz$dirEncode$ts.jpg $dirRaiz$dirUpload
rm $dirRaiz$dirCaptura$nombre
